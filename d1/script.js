console.log('Array Manipulation')

// Basic Array Structure
// Access Elements in an Array - through index

// two ways to initialize an array

let array = [1,2,3];

console.log(array);

let arr = new Array(1,2,3)
console.log(arr);


console.log(array[0])
console.log(array[1])
console.log(array[2])

// array manipulation

let count = ['one','two','three','four']

console.log(count.length)
console.log(count[4])

// adding element to array using assigment operator
// can ONLY add in the end of the array
count[4]='five'
console.log(count[4])

// push method array.push()
// add to the end of the array
count.push('element')
console.log(count)

function pushMethod(element) {
	return count.push(element)

}

pushMethod('six')
pushMethod('seven')
pushMethod('eight')

console.log(count)


// pop method array.pop
count.pop()
console.log(count)

// removed 
count.pop('four')
console.log(count)


function popMethod(){
	count.pop()
}
popMethod()
console.log(count)


// add element at the beginning of an array
// unshift method

count.unshift('hugot')
console.log(count)

function unshiftMethod (name) {
	return count.unshift(name)

}

unshiftMethod('zero')

console.log(count)

// remove from the beginning of the array
count.shift()
console.log(count)

function shiftMethod () {
	return count.shift()

}

shiftMethod()

console.log(count)


// sort method array.sort()
let nums = [15,32,61,130,230,13,34]
nums.sort()
console.log(nums)

// sort nums in ascending order CALLBACK FUNCTION
nums.sort( 
		function (a,b){
			return a - b
		}
	)

console.log(nums)

// descending order
nums.sort( 
		function (a,b){
			return b - a
		}
	)

console.log(nums)


// reverse method array.reverse()
// flips the order NOT SORT
nums.reverse()
console.log(nums)

// splice and slice to remove element in the middle on an array

// splice method array.splice
// returns the array of omitted elements
// directly manipulate the target array
// accepted parameter
/*
	first parameter - where to start omitting elements
	second parameter - # of elements to be omitted starting from the first parameter
	third parameter - elements to be added in place of the omitted elements
*/


console.log(count)
// ommit array element starting from index 1 
// let newSplice = count.splice(1)
// console.log(newSplice)
// console.log(count)

// ommit 2 array element starting from index 1, 
// let newSplice = count.splice(1,2)
// console.log(newSplice)
// console.log(count)

// ommit 2 array element starting from index 1, 
// let newSplice = count.splice(1,2, 'a1', 'b2', 'c3')
// console.log(newSplice)
// console.log(count)



// slice method array.slice(starting index, element#)
// returns the remaining elements
// does not directly affects the original array. it creates a copy of the original

/*
	first parameter - where to start omitting parameter
	second parameter - until what element # to COPY. this is not index hence 1 = index 0
	third parameter - elements to be added in place of the omitted elements
*/

console.log('slice')
console.log(count)

// remove the elements prior to index 1
// let newSlice = count.slice(1)
// console.log(newSlice)
// console.log(count)


let newSlice = count.slice(1,6)
console.log('newslice ' + newSlice)
console.log(count)

// splice is removing and adding in the array
// slice is like getting a copy of a part of the array

// concat method array.concat()
// to merge 2 or more arrays
console.log(count)
console.log(nums)
let animals = ['bird','cat','dog','fish']

let newConcat = count.concat(nums,animals)
console.log(newConcat)


// join method array.join()
// to join elements to form a 1 string
// comma is default separator
let meal = ['rice', 'steak','juice']

let newJoin = meal.join()

console.log(newJoin)

newJoin = meal.join("")
console.log(newJoin)

newJoin = meal.join(" ")
console.log(newJoin)

newJoin = meal.join("-")
console.log(newJoin)



// toString method
console.log(nums)
console.log(typeof nums[3])


let newString = nums.toString()
console.log(typeof newString)

// typeof use to know what datatype

// ======================
// accessors
let countries = ['US', 'PH','CAN','PH' ,'SG' ,'HK' ,'PH' ,'NZ']
// indexOf() find the index of a given element where it is first found

 let index = countries.indexOf('PH')
 console.log(index)

// if element is non existing it will return -1
  let sample = countries.indexOf('EU')
 console.log(sample)

/*if (countries.indexOf('CAN') === -1) {
	console.log('Element not existing')
}

else {
	console.log('Element exist in the countries array')
}*/



// lastIndexOf() find the index of a given element where it is first found
 let lastIndex = countries.lastIndexOf('PH')
  console.log(lastIndex)

// ======================
// Iterators

// forEach(callbackfuntion()) array.forEach()
// to loop in the array. executes statements for each array element. to display the array elements. returns UNDEFINED. multiple line output





let days = ['mon', 'tue' , 'wed' , 'thu', 'fri' , 'sat' , 'sun']


let daysOfTheWeek = days.forEach(
		function(element) {
			console.log(element)
		}
	)

// will return UNDEFINED
console.log('days of the week:' + daysOfTheWeek) 


// map()  array.map() returns a copy of array that can be modified one line output

// let mapDays = days.map(
// 		function(day) {
			
// 			return `${day} is the day of the week`
// 			// template literals `${}`
// 		}
// 	)

// console.log(mapDays)
// console.log(days)

// mini activity
console.log('mini activity')
let days2=[]; 
console.log(days2)
days.forEach(
		function(day) {
			days2.push(day)
		}
	)

console.log(days2)

console.log('end of mini activity')

console.log(nums)

let newFilter = nums.filter(function (element) {
		if (element < 50){
			return (element)
		}
})


console.log('newfilter: ' + newFilter)


// includes array.includes() RETURN BOOLEAN VALUES
console.log(animals)


let newIncludes = animals.includes("cat")

console.log(newIncludes)


console.log('mini activity')



function fanimal(a) {
	if (animals.includes(a)) {
	console.log('Value is found')

	}
	else {
	console.log('Value is not found')
	}
}


fanimal("cat")
fanimal("lion")
fanimal("tiger")

console.log('end of mini activity')


// every (BOOLEAN) array.every(cb())
// returns TRUE if ALL ELEMENTS passed the given condition. like &&

let newEvery = nums.every(function(num){return (num >1)})

console.log(newEvery)

// some (BOOLEAN) array.some(cb())
// returns TRUE if SOME ELEMENTS passed the given condition. like ||

let newSome = nums.every(function(num){return (num > 1)})

console.log(newSome)

// reduce(cb(<prev value>,<current value>))
// like sort pero with arithmetic 

let newReduce = nums.reduce(function(a,b){
	return a+b
	// return b-a
})
console.log(newReduce)

// to get the average of nums array
// total all the elements
// divide it by total number of elements


let average = newReduce/nums.length

// toFixed(# of decimals) returns string

console.log(typeof average.toFixed(2))

// parseInt to convert to number but will REMOVE decimal
// parseFloat to convert to number but will RETAIN decimal
console.log(parseInt(average))
console.log(parseInt(average.toFixed(2)))
console.log(parseFloat(average.toFixed(2)))