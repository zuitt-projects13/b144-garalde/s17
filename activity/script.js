let student = [];

function addStudent(name) {
	student.push(name)
	console.log(`Student ${name} was successfully added.`)
}

function getStudentName() {
	addStudent(prompt("Input student name: "))
}

/*addStudent('maryann');
addStudent('anne');
addStudent('abby');
addStudent('may');
addStudent('fei');
addStudent('janiel');*/

// student name input
for (let i = 0; i <5;i++){
getStudentName()
}


function countStudents() {
	return student.length
}

console.log('Total number of students: ' + countStudents())

function printStudent() {
	student.sort()
	student.forEach((function (name) {
		console.log(name)
	}))
}


printStudent()


// find student============



function findStudent(key) {

	// Step 6a
	let keyword = key.toLowerCase()
	let filterMatch = student.filter(
		function (studentName) { 
			// each element of student array will be entered into the function to check if it contains the keyword
			//if not equal to -1 means that keyword is available in the array
			//the returned studentName will be pushed to filterMatch
			return studentName.indexOf(keyword) !== -1; }
		);

	// Step 6a to 6e
	let foundMatch = filterMatch.length;
	if (foundMatch === 1){
		console.log(`${filterMatch} is an enrollee.`)
	}
	else if (foundMatch > 1) {
		console.log(`${filterMatch} are enrollees.`)
	}

	else if (foundMatch === 0) {
		console.log(`${key} is not an enrollee.`)
	}

	else {
		console.log('girl may something wrong')
	}
}


let filterKey = prompt("Enter keyword to search Student Name: ")
findStudent(filterKey)

// STRETCH GOALS=========
console.log('STRETCH GOALS=========')
function addSection(sectionName) {
	let withSection = student.map(
			function (studentName){return `${studentName} - ${sectionName}`}
		)
	console.log(withSection)
}

addSection('Section A')
console.log(student)

